var CRouter = require('express').Router();
var CashierController = require('./CashierController.js');

CRouter.route('/')
.post(function(req,res){
	CashierController.newCashier(req,res);
})
.get(function(req,res){
	CashierController.retrive(req,res);
})
CRouter.route('/login')
.post(function(req,res){
	CashierController.login(req,res);
})

module.exports = CRouter ;
