var TRouter = require('express').Router();
var TransactionController = require('./TransactionController.js');

TRouter.route('/')
.get(function(req,res){
TransactionController.retrive(req,res);
})

TRouter.route('/create')
.post(function(req,res){
	TransactionController.create(req,res);
})
TRouter.route('/retriveForCa/')
.get(function(req,res){
	TransactionController.retriveForCashier(req,res);
})
module.exports =TRouter;