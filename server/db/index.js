var mysql = require('mysql');

var db = mysql.createConnection({
	host:'localhost',
	user:'root',
	password:'',
	database:'PoS'
});

db.connect(function(err){
  if(err){
    console.log('Error connecting to Db');
    return;
  }
  console.log('Connection');
});

module.exports = db;