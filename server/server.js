const express = require('express');
var bodyParser = require('body-parser');
var db =require('./db/index.js');
var Cashier = require('./resources/Cashier/CashierRouter.js');
var Transaction = require('./resources/Transaction/TransactionRouter.js');
const app = express();
var cors = require('cors')

app.use(cors())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));

 app.use(express.static(__dirname + '/../dist'));

app.use('/cashier',Cashier);
app.use('/transaction',Transaction);
module.exports = app;