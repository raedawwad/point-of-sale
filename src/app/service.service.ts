import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { map } from "rxjs/operators";
import { HttpHeaders, HttpClient} from '@angular/common/http';

 var serverApi= 'http://localhost:3000';
@Injectable({
  providedIn: 'root'
})
export class ServiceService {
 constructor(private http: Http,private httpClient: HttpClient) { }
 data: any = [];
  // Get all posts from the API
  getAllData() {
    return this.http.get(`${serverApi}/transaction/`).pipe(map(res => res.json()));
  }
  getCashier(){
  	return this.http.get(`${serverApi}/cashier/`).pipe(map(res => res.json()));
  }


  getTeansaction(id){
 return this.httpClient.get(`${serverApi}/transaction/retriveForCa`, {
      params: {
        id: id
      },
      observe: 'response'
    })
    .toPromise()
    .then(response => {
      this.data = response.body;
    })
    .catch(console.log);
   
}
Retrive(){
	return this.data;
}
}