import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceService } from "./service.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
	datas: any = [];
constructor(private _service: ServiceService, private router: Router) { }
  title = 'app';
  ngOnInit() {
        this._service.getAllData().subscribe(data => {
        this.datas = data;
  });
}
}
